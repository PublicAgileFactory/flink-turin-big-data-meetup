---
theme : "night"
transition: "slide"
highlightTheme: "monokai"
logoImg: "assets/logo.png"
slideNumber: true
title: "Apache Flink - Stateful streaming done right"
---

# Apache Flink 

### Stateful streaming done right 


Torino Big Data meetup

7 Luglio 2020 @ Home

----

#### Speaker

Andrea Fonti

<img width="30%" src="./assets/mecropped.png">

Big Data Engineer @ AgileLab

----

<blockquote  class="twitter-tweet" data-dnt="true" data-theme="dark"><p lang="en" dir="ltr">Best <a href="https://twitter.com/hashtag/fp?src=hash&amp;ref_src=twsrc%5Etfw">#fp</a> tweet ever <a href="https://t.co/V8qHo4iUW9">https://t.co/V8qHo4iUW9</a></p>&mdash; Andrea Fonti (@andrea_rockt) <a href="https://twitter.com/andrea_rockt/status/1243475560686407680?ref_src=twsrc%5Etfw">March 27, 2020</a></blockquote>

----

#### Agile Lab

<img width="30%" src="./assets/logo.png"/>

An Italian company 

focused on scalable technologies and big data

Streaming | IoT | AI | Hadoop

[www.agilelab.it](http://www.agilelab.it)

---

# Agenda

- Un caso d'uso stateful
- Overview flink
    - Architettura
    - Modalità di deploy 
- Gestione dello stato 
    - API 
    - Introspezione 
    - Pre popolamento 

---

## Un caso d'uso stateful

----

![stateful-usecase-diagram](./assets/usecase2.svg)

----

## Change data capture

* Stream ordinato di aggiornamenti incrementali ad un entità
* Le entità sono identificate dalla loro chiave, Primary Key nel mondo SQL

```scala
sealed trait Change[E]
sealed trait Mutation[K, E]
case class Insert(key: K, changes: List[Change[E]])
     extends Mutation[K, E]
case class Upsert(key: K, changes: List[Change[E]])
     extends Mutation[K, E]
case class Delete(key: K) extends Mutation[K, E]
```

è necessario applicare i cambiamenti in ordine

----

## Ordinamento su kafka

![cdc-diagram](./assets/usecase3.svg)

----

## Trasformazione 

<img width="50%" src="./assets/usecase4.svg"/>

----

<img width="45%" style="padding:20px" src="./assets/showcode.jpg"/>

----

```scala
val anagraficaStream: DataStream[Mutation[KA, Anagrafica]] = ???
val contattiStream: DataStream[Mutation[KC, Contatti]] = ???
val databaseSink: SinkFunction[ProfileWithOperation] = ???
val statefulJoin: KeyedCoProcessFunction[JoinKey,
                                   Mutation[KA, Anagrafica],
                                   Mutation[KC, Contatti],
                                   ProfileWithOperation] = ???
anagraficaStream.forward
  .connect(contattiStream.forward)
  .keyBy(anagrafica => joinKey(anagrafica) 
         contatto => joinKey(contatto))
  .process(statefulJoin)
  .addSink(databaseSink)

flinkEnvironment.execute()
```
----

```scala
class StatefulProcessorRichFunction
    extends KeyedCoProcessFunction[JoinKey,
                                   Mutation[KA, Anagrafica],
                                   Mutation[KC, Contatti],
                                   ProfileWithOperation] {

  type Context = KeyedCoProcessFunction[JoinKey,
    Mutation[KA, Anagrafica],
    Mutation[KC, Contatti],
    ProfileWithOperation]#Context

  @transient private var state: ValueState[ProcessorState] = _

  override def open(parameters: Configuration): Unit = {

    val info = createTypeInformation[ProcessorState]

    val descriptor = new ValueStateDescriptor[ProcessorState]
        ("processor-state", info)

    state = getRuntimeContext.getState(descriptor)

  }

  override def processElement1(
      value: Mutation[KA, Anagrafica], 
      ctx: Context,
      out: Collector[ProfileWithOperation]
  ): Unit = {

    val k = ctx.getCurrentKey

    handleAnagrafica(value) match {
      case Left(error) => 
        throw new RuntimeException(error.toString)
      case Right((newState, None)) =>
        state.update(newState)
      case Right((newState, Some(data))) =>
        state.update(newState)
        out.collect(data)
    }

  }

  override def processElement2(
      value: Mutation[KC, Contatti],
      ctx: Context, 
      out: Collector[UniqueProfileWithOperation]
  ): Unit = {

    val k = ctx.getCurrentKey

    handleContatti(value) match {
      case Left(error) => 
        throw new RuntimeException(error.toString)
      case Right((newState, None)) =>
        state.update(newState)
      case Right((newState, Some(data))) =>
        state.update(newState)
        out.collect(data)
    }

  }

}
```

----

```scala
class RepoSink(factory: () => Repository[Profile]) 
    extends RichSinkFunction[ProfileWithOperation] {

  var repository: Repository[Profile] = _

  override def invoke(value: ProfileWithOperation): Unit = {

    val result = value match {
      case ProfileWithOperation(up, Insert) =>
        repository.insert(up)
      case ProfileWithOperation(up, Upsert) =>
        repository.upsert(up)
      case ProfileWithOperation(up, Delete) =>
        val pk = pk(up) 
        repository.delete(pk)
    }

    result match {
      case Right(_) =>
      case Left(error) =>
        throw new RuntimeException(error.toString)
    }

  }

  override def open(parameters: Configuration): Unit = {
    repository = factory()
  }

}

```
---

## Flink overview

----

## Architettura

<img width="70%" style="padding:20px;filter: invert(100%);" src="./assets/flink-architecture.svg"/>

----

## Operators and Tasks 


<img width="60%" style="padding:20px;filter: invert(100%);" src="./assets/tasks-chains.svg"/>


----

## Task slots

<img width="80%" style="padding:20px;filter: invert(100%);" src="./assets/tasks-slots.svg"/>

----

## Slots sharing


<img width="80%" style="padding:20px;filter: invert(100%);" src="./assets/slot-sharing.svg"/>

----

## Checkpointing


<img width="60%" style="padding:20px;filter: invert(100%);" src="./assets/checkpoints.svg"/>

----

## Modalità di deployment

----

## Standalone


<img width="60%" style="padding:20px" src="./assets/standalone.svg"/>

----

## Standalone HA


<img width="60%" style="padding:20px" src="./assets/ha.svg"/>

----


<img width="45%" style="padding:20px" src="./assets/meme.jpg"/>

----

## Yarn Session

<img width="45%" style="padding:20px" src="./assets/yarn.svg"/>

----

## Yarn Job

<img width="45%" style="padding:20px" src="./assets/yarn-job.svg"/>

----

<img width="45%" style="padding:20px" src="./assets/yarn-job-2.svg"/>


---

## Gestione dello stato

----

<img width="45%" style="padding:20px" src="./assets/spark-vs-flink-meme.jpg"/>

----

## Flink state 

* Interrogabile
* Evolvibile
* Inizializzabile
* Scala automaticamente con il parallelismo
* Backend multipli

----

## Spark state 

* Funziona*

----

## State types

----

### `ValueState<T>`

Mantiene un valore che può essere aggiornato e recuperato 

----

### `ListState<T>`

Mantiene una lista di elementi. Si possono aggiungere elementi e recuperare un `Iterable` su tutti gli elementi attualmente memorizzati.

----

### `ReducingState<T>`

Mantiene un unico valore che rappresenta l'aggregazione di tutti i valori aggiunti allo stato.

L'interfaccia è simile a `ListState` ma gli elementi aggiunti sono ridotti ad un aggregato usando una specifica `ReduceFunction`.

----

### `AggregatingState<IN, OUT>`

Mantiene un singolo valore che rappresenta l'aggregazione di tutti i valori aggiunti allo stato. 

Contrariamente a `ReducingState`, il tipo di aggregato può essere diverso dal tipo di elementi che vengono aggiunti allo stato. 

L'interfaccia è la stessa di `ListState` ma gli elementi aggiunti usando sono aggregati usando una specifica `AggregateFunction`.

----

### `FoldingState<T, ACC>`

Mantiene un unico valore che rappresenta l'aggregazione di tutti i valori aggiunti allo stato

Contrariamente a `ReducingState`, il tipo di aggregato può essere diverso dal tipo di elementi che vengono aggiunti allo stato.

L'interfaccia è simile a `ListState` ma gli elementi aggiunti sono schiacciati in un aggregato usando una specifica `FoldFunction`.

----

### `MapState<UK, UV>`

Mantiene una lista di relazioni chiave valore. 

Si possono mettere coppie chiave-valore nello stato e recuperare un `Iterable` su tutte le relazioni attualmente memorizzate. 

----

### Operator State

Non è definito in relazione alla chiave dell'elemento di ingresso

Bisogna selezionare una strategia di redistribuzione

* Even Split
* Union

----

### Checkpointing

<img  style="padding:20px;filter: invert(100%);" src="./assets/stream_barriers.svg"/>

----

<img style="padding:20px;filter: invert(100%);" src="./assets/stream_aligning.svg"/>

----

<img style="padding:20px;filter: invert(100%);" src="./assets/checkpointing.svg"/>

----



```scala
val env: StreamExecutionEnvironment = 
    StreamExecutionEnvironment.getExecutionEnvironment();

// start a checkpoint every 1000 ms
env.enableCheckpointing(1000);

// advanced options:

// set mode to exactly-once (this is the default)
env.getCheckpointConfig()
    .setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);

// make sure 500 ms of progress happen between checkpoints
env.getCheckpointConfig()
    .setMinPauseBetweenCheckpoints(500);

// checkpoints have to complete within one minute, or are
// discarded
env.getCheckpointConfig()
    .setCheckpointTimeout(60000);

// allow only one checkpoint to be in progress at the same
// time
env.getCheckpointConfig()
    .setMaxConcurrentCheckpoints(1);

// enable externalized checkpoints which are retained after
// job cancellation
env.getCheckpointConfig()
    .enableExternalizedCheckpoints(
        ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);

// allow job recovery fallback to checkpoint when there is
// a more recent savepoint
env.getCheckpointConfig()
    .setPreferCheckpointForRecovery(true);

// enables the experimental unaligned checkpoints
env.getCheckpointConfig
    .enableUnalignedCheckpoints();
```

----

### Queryable state

```scala
val descriptor =
        new ValueStateDescriptor[(Long,Long)](
                "average", 
                TypeInformation.of[(Long,Long)]);
descriptor.setQueryable("query-name");

val client = new QueryableStateClient(tmHostname, proxyPort);
client.getKvState(jobId,
                 "query-name",
                 key,
                 BasicTypeInfo.LONG_TYPE_INFO,
                 descriptor);
```
----

### Manipolare lo stato 

```scala
val bEnv      = ExecutionEnvironment.getExecutionEnvironment
val savepoint = Savepoint.load(bEnv, 
                               "hdfs://path/",
                                new MemoryStateBackend)
```

----

### Bootstrap

```scala
case class Account(id: Int, amount: Double, timestamp: Long)

class AccountBootstrapper 
    extends KeyedStateBootstrapFunction[Integer, Account] {
    var state: ValueState[Double]

    @throws[Exception]
    override def open(parameters: Configuration): Unit = {
        val descriptor = 
            new ValueStateDescriptor("total",Types.DOUBLE)
        state = getRuntimeContext().getState(descriptor)
    }

    @throws[Exception]
    override def processElement(value: Account, ctx: Context)
        : Unit = {
        state.update(value.amount)
    }
}
 
val bEnv = ExecutionEnvironment.getExecutionEnvironment()

val accountDataSet = bEnv.fromCollection(accounts)

val transformation = OperatorTransformation
    .bootstrapWith(accountDataSet)
    .keyBy(acc => acc.id)
    .transform(new AccountBootstrapper)
```

----

### State backends

* Memory
* Filesystem
* RocksDB

Può essere memorizzato su hdfs

---

# Recap

Flink offre:

* Semantiche chiare
* Evolvibilità, rescaling ed introspezione dello stato
* Modalità di deploy flessibili
* Checkpoints e savepoints
* Team ops felici

----

## The ops team before

<img width="40%" src="./assets/ops_upgrade.jpg"/>

----

## The ops team after

<img width="40%" src="./assets/ops_upgrade_flink.jpg"/>


---

The END?

----

<img width="40%" src="./assets/ops_upgrade.jpg"/>
